﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace Awesome_Trading_Game
{
    public partial class Form1 : Form
    {

        String townpath = Application.StartupPath + "\\map.txt";
        String itempath = Application.StartupPath + "\\items.txt";

        private struct tradeItems
        {
            public int itemID;
            public string itemName;
            public int itemPrice;
            public string itemImage;
        }

        tradeItems[] inventory = new tradeItems[6];
        Label[] arrItemNames = new Label[6];
        Label[] arrItemQty = new Label[6];
        Label[] arrItemPrice = new Label[6];

        List<Town> towns = new List<Town>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //createInventory();
            setupScreen();
            read_file(townpath);
            read_items(itempath);
            
            loadScreen();

            // intialise game by loading town 0
            loadTown(0);

        }

        // Method to load in data for a particular town based
        // on the ID passed to it
        private void loadTown(int townID)
        {
            // Display city name
            lblCity.Text = towns[townID].getName();

            // Get list of prices
            int[] prices = new int[6];
            prices = towns[townID].getListOfPrices();

            // Display list of prices
            // Note: you will need to change the indexes here
            //       to include the new prices that you add
            for(int i = 0; i <= 1; i++)
            {
                arrItemPrice[i].Text = prices[i].ToString();
            }
        }

        private void read_file(String filepath)
        {
            foreach (string line in File.ReadLines(filepath))
            {
                if (line.StartsWith("#") == false) // allows comments in the file
                {
                    String[] tmp = new String[14];
                    tmp = line.Split('|'); // splits the line of text on a '|' character

                    // Create a new Town object and fill it in with data
                    // from our file
                    towns.Add(new Town {
                        name = tmp[1],
                        six_shooter_base = int.Parse(tmp[2]),
                        six_shooter_low = int.Parse(tmp[3]),
                        six_shooter_high = int.Parse(tmp[4]),
                        shovels_base = int.Parse(tmp[5]),
                        shovels_low = int.Parse(tmp[6]),
                        shovels_high = int.Parse(tmp[7])

                        // You'll have to add the rest here

                    } );
                } 
            }
        }

        // Read in the list of items we are trading
        private void read_items(String filepath)
        {
            int i = 0;
            foreach (string line in File.ReadLines(filepath))
            {
                if (line.StartsWith("#") == false)
                {
                    String[] tmp = new String[2];
                    tmp = line.Split('|');
                    inventory[i].itemName = tmp[0];
                    i++;
                }
            }
        }

        private void loadScreen()
        {
            int i = 0;
            foreach (Label item in arrItemNames)
            {
                item.Text = inventory[i].itemName;
                i++;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void setupScreen()
        {

            // Set up arrays of screen elements to make our
            // lives easier. Ideally we would create these at
            // runtime, but hey, I'm not going to do everything
            // for you.
            arrItemNames[0] = lblItem1;
            arrItemNames[1] = lblItem2;
            arrItemNames[2] = lblItem3;
            arrItemNames[3] = lblItem4;
            arrItemNames[4] = lblItem5;
            arrItemNames[5] = lbItem6;

            arrItemPrice[0] = lblPrice1;
            arrItemPrice[1] = lblPrice2;
            arrItemPrice[2] = lblPrice3;
            arrItemPrice[3] = lblPrice4;
            arrItemPrice[4] = lblPrice5;
            arrItemPrice[5] = lblPrice6;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
